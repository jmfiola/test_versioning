import os

from setuptools import find_namespace_packages, setup


os.chdir(os.path.abspath(os.path.dirname(__file__)))

HERE = os.path.dirname(__file__)
VERSION_FILE = os.path.join(HERE, 'VERSION.txt')
setup(
    name="cobs_example",
    version_config={
        "count_commits_from_version_file": True,
        "dev_template": "{branch}.{ccount}",
        "dirty_template": "{branch}.{ccount}",
        "version_file": VERSION_FILE
    },
    setup_requires=['setuptools-git-versioning'],
    description="SomeDescription",
    long_description="{0}".format(open("README.rst").read()),
    author="Jacob Fiola",
    author_email="example@example.com",
    packages=find_namespace_packages("src"),
    include_package_data=True,
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "License :: Other/Proprietary License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python"],
    package_dir={"": "src"},
    entry_points={"console_scripts": []}
)
